import java.util.Scanner;

public class FibonacciGenerator 
	{
		public static void validateN(int x)
		{
			String Input = "";
			try {
				int intValue = Integer.parseInt(Input);
			}catch(NumberFormatException e) {
				System.out.println("Input is not a valid integer");
			}
		}
		
		public static int calculateNthValue(int n)
		{
			int f[] = new int[n+1];
			    f[0] = 0;
				f[1] = 1;
			for (int i = 2; i <= n; i++)
			{
				f[i] = f[i-1] + f[i-2];
			}
			return f[n];
	    }
		
		
		public static void main (String args[]) throws Exception
		{
		    	Scanner input = new Scanner(System.in); 
		    	System.out.print("enter n value : ");
		    	int n = input.nextInt();
				validateN(n);
				int result = calculateNthValue(n);
				System.out.println("Fibonacci number for your input is :  "+ result);
		}
	}
